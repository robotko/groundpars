<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$loader = require __DIR__.'/vendor/autoload.php';

require_once __DIR__.'/config.php';

$pdo = new DB();
$pdo = $pdo->execute();
$mailer = new PHPMailer;
$phpexcel = new PHPExcel;
$parser = new \Grundpars\Parser($pdo,$mailer,$phpexcel);
$parser->start();

?>