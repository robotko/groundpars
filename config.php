<?php 

class DB
{
	public $pdo;

	public function execute()
	{
   		$host = 'localhost';
   		$dbname = 'test';
   		$user = 'root';
   		$pass = '';
		$pdo = new \PDO("mysql:host=$host;dbname=$dbname", $user, $pass) ;
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo->exec('SET NAMES "utf8"');
		return $pdo;
	}
}
